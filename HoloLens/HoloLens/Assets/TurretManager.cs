﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class TurretManager : MonoBehaviour, INavigationHandler
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnNavigationStarted(NavigationEventData eventData)
    {

    }

    public void OnNavigationUpdated(NavigationEventData eventData)
    {
        var rotationFactory = eventData.CumulativeDelta.x * 10.0f;
        transform.Rotate(0, rotationFactory, 0);
    }

    public void OnNavigationCompleted(NavigationEventData eventData)
    {

    }

    public void OnNavigationCanceled(NavigationEventData eventData)
    {

    }
}
