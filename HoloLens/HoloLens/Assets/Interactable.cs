﻿using System.Collections;
using System.Collections.Generic;
using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class Interactable : MonoBehaviour, IFocusable
{
    public Color FocusedColor = Color.red;
    private Material cachedMaterial;
    private Color originalColor;
    void Awake()
    {
        cachedMaterial = GetComponent<Renderer>().material;
        originalColor = cachedMaterial.GetColor("_Color");
    }

    public void OnFocusEnter()
    {
        cachedMaterial.SetColor("_Color", FocusedColor);
    }

    public void OnFocusExit()
    {
        cachedMaterial.SetColor("_Color", originalColor);
    }
}
