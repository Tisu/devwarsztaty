﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingMAnager : MonoBehaviour
{
    public Rigidbody ShellPrefab;
    public Transform FiringTransform;
    public AudioClip ShootingAudio;
    private AudioSource _audioSource;
    // Use this for initialization
    void Awake()
    {
        _audioSource = gameObject.AddComponent<AudioSource>();
        _audioSource.clip = ShootingAudio;
        _audioSource.playOnAwake = false;
        _audioSource.loop = false;
        _audioSource.spatialize = true;
        _audioSource.spatialBlend = 1.0f;
        _audioSource.volume = 1.0f;
    }

    public void Fire()
    {
        var shellInstance = Instantiate(ShellPrefab, FiringTransform.position, FiringTransform.rotation);
        _audioSource.Play();
        shellInstance.AddForce(FiringTransform.forward * 1.0f, ForceMode.Impulse);
    }
    
}
