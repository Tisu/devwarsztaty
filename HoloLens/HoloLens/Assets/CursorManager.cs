﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;
namespace Assets
{
    public class CursorManager : MonoBehaviour
    {
      
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (GazeManager.Instance.IsGazingAtObject)
            {
                gameObject.transform.position = GazeManager.Instance.HitInfo.point;
                gameObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, GazeManager.Instance.HitInfo.normal);
            }
            else
            {
                
            }
        }
    }
}
