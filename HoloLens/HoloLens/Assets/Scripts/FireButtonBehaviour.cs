﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using System;

public class FireButtonBehaviour : MonoBehaviour, IInputClickHandler  {

    public ShootingBehaviour ShootingBehaviour;

    private void Start()
    {
        Debug.Log("START");
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        ShootingBehaviour.Fire();
    }
}
