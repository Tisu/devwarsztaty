﻿using System;
using HoloToolkit.Unity.InputModule;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class Interactible : MonoBehaviour, IFocusable {

    [Tooltip("Object color changes to this when focused.")]
    public Color FocusedColor = Color.red;

    private Color originalColor;
    private Material cachedMaterial;

    public void Awake()
    {
        cachedMaterial = GetComponent<Renderer>().material;
        originalColor = cachedMaterial.GetColor("_Color");
    }

    private void OnDestroy()
    {
        DestroyImmediate(cachedMaterial);
    }

    public void OnFocusEnter()
    {
        cachedMaterial.SetColor("_Color", FocusedColor);
    }

    public void OnFocusExit()
    {
        cachedMaterial.SetColor("_Color", originalColor);
    }
}
