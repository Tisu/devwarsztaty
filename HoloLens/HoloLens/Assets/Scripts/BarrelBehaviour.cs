﻿using System;
using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class BarrelBehaviour : MonoBehaviour, INavigationHandler {
    public void OnNavigationCanceled(NavigationEventData eventData)
    {
        
    }

    public void OnNavigationCompleted(NavigationEventData eventData)
    {
       
    }

    public void OnNavigationStarted(NavigationEventData eventData)
    {
       
    }

    public void OnNavigationUpdated(NavigationEventData eventData)
    {
        var rotationFactorX = eventData.CumulativeDelta.y * 10.0f;
        transform.Rotate(new Vector3(rotationFactorX, 0, 0));
    }
}
