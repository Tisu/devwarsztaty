﻿using UnityEngine;

public class ShootingBehaviour : MonoBehaviour {

    public Rigidbody ShellPrefab;
    public Transform FiringTransform;
    public AudioClip ShotFiring;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = gameObject.AddComponent<AudioSource>();

        _audioSource.clip = ShotFiring;
        _audioSource.playOnAwake = false;
        _audioSource.loop = false;
        _audioSource.spatialize = true;
        _audioSource.spatialBlend = 1.0f;
        _audioSource.volume = 1.0f;
    }

    public void Fire()
    {
        var shellInstance = Instantiate(ShellPrefab, FiringTransform.position, FiringTransform.rotation);

        _audioSource.Play();

        shellInstance.AddForce(FiringTransform.forward * 1.0f, ForceMode.Impulse);
    }
}
