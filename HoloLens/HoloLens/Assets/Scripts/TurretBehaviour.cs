﻿using HoloToolkit.Unity.InputModule;
using UnityEngine;

public class TurretBehaviour : MonoBehaviour, INavigationHandler {

    public void OnNavigationCanceled(NavigationEventData eventData)
    {
       
    }

    public void OnNavigationCompleted(NavigationEventData eventData)
    {
      
    }

    public void OnNavigationStarted(NavigationEventData eventData)
    {
        
    }

    public void OnNavigationUpdated(NavigationEventData eventData)
    {
        var rotationFactorY = eventData.CumulativeDelta.x * 10.0f;
        transform.Rotate(new Vector3(0, rotationFactorY, 0));
    }
}
